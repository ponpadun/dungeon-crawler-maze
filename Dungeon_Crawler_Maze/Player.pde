// assign player movement variables
boolean leftMovement = false;
boolean rightMovement = false;
boolean upMovement = false;
boolean downMovement = false;
PImage player;

// assign speed and frictin value for player
float playerSpeed = 0.85;
float playerFriction = 0.5;

class Player
{      
  // assign PVector variables for player's movement when the value changes
  PVector playerLocation = new PVector (0,0);
  PVector playerVelocity = new PVector (0,0);
  PImage playerImage=loadImage("player.png");
    Player ()
    {
      // assign starting position for player
      playerLocation.x = 300;
      playerLocation.y = 300;
      player = loadImage("player1.png");
    }
    
    // main fuction for player class
    void playerCharacter ()
    {     
        updatePlayer();
        playerConstraint();
        displayPlayer();
        pickupCollision();
    }
    
    /* 
    make player moves by using variables that is assigned
    when a keyboard key is pressed and player's input is registered
    */
    void updatePlayer()
    {
       if (keyPressed)
       {
         // up movement by decreasing the y value
         if (upMovement == true)
         {
           playerVelocity.y = -playerSpeed;
         }
         // down movement by increasing the y value
         else if (downMovement == true)
         {
           playerVelocity.y = playerSpeed;
         }
         
         else         
         playerVelocity.y *= playerFriction;
         // left movement by decreasing the x value
         if (leftMovement == true)
         {
           playerVelocity.x = -playerSpeed;
         }
         // right movement by increasing the x value
         else if (rightMovement == true)
         {
           playerVelocity.x = playerSpeed;
         }
         
         else
           // multiple the friction when player is not moving for smooth movement and stop
           playerVelocity.x *= playerFriction;
       }
       
      else playerVelocity.mult(playerFriction);
      
      playerLocation.add(playerVelocity);
    }
    
    // function for the constraint of player position
    void playerConstraint ()
    {
      // constraint on the left side
      if (playerLocation.x < 10)
      {
        playerLocation.x = 10;
      }
      // constraint on the right side
      else if (playerLocation.x > 590)
      {
        playerLocation.x = 590;
      }
      // constraint on the top side
      if (playerLocation.y < 10 )
      {
        playerLocation.y = 10;
      }
      // constraint on the bottom side
      else if (playerLocation.y > 550)
      {
        playerLocation.y = 550;
      }
    }
    
    // function to display player visual 
    void displayPlayer()
    {        
      if(leftMovement)
      {
        animate.playerleft();
      }
      else if(rightMovement)
      {
      animate.playerright();
      }
      else if(upMovement){
        animate.playerback();
      }
      else if(downMovement){
        animate.playerfront();
      }
      else
      {
        animate.playerfront();
      }
    }
    
    void damage(){
    
}

      void pickupCollision()
  { 
    ///////////////////////// AMMO ///////////////////////////////    
    if (ammo1IsPickedup == false && currentAmmo < 15)
    {
      
      if (playerLocation.x > ammoPickupLocation1.x && playerLocation.x < ammoPickupLocation1.x+50 && playerLocation.y > ammoPickupLocation1.y-50 && playerLocation.y < ammoPickupLocation1.y+50)
      {
        ammo1IsPickedup = true;
        currentAmmo = 15;
      }
    }  
    
    if (ammo2IsPickedup == false && currentAmmo < 15)
    {
      
      if (playerLocation.x > ammoPickupLocation2.x && playerLocation.x < ammoPickupLocation2.x+50 && playerLocation.y > ammoPickupLocation2.y-50 && playerLocation.y < ammoPickupLocation2.y+50)
      {
        ammo2IsPickedup = true;
        currentAmmo = 15;
      }
    }  

    if (ammo3IsPickedup == false && currentAmmo < 15)
    {
      
      if (playerLocation.x > ammoPickupLocation3.x && playerLocation.x < ammoPickupLocation3.x+50 && playerLocation.y > ammoPickupLocation3.y-50 && playerLocation.y < ammoPickupLocation3.y+50)
      {
        ammo3IsPickedup = true;
        currentAmmo = 15;
      }
    }  
    ////////////////////////////// HEALTH ////////////////////////////
    if (health1IsPickedup == false)
    {
    if (playerLocation.x > healthPickupLocation1.x && playerLocation.x < healthPickupLocation1.x+50 && playerLocation.y > healthPickupLocation1.y-50 && playerLocation.y < healthPickupLocation1.y+50 && playerHealth < 50)
      {
        health1IsPickedup = true;
        playerHealth = playerHealth + 10;
      }   
    }
    
    if (health2IsPickedup == false)
    {
      if (playerLocation.x > healthPickupLocation2.x && playerLocation.x < healthPickupLocation2.x+50 && playerLocation.y > healthPickupLocation2.y-50 && playerLocation.y < healthPickupLocation2.y+50 && playerHealth < 50)
      {
        health2IsPickedup = true;
        playerHealth = playerHealth + 10;
      }
    }
    if (health3IsPickedup == false)
    {
      if (playerLocation.x > healthPickupLocation3.x && playerLocation.x < healthPickupLocation3.x+50 && playerLocation.y > healthPickupLocation3.y-50 && playerLocation.y < healthPickupLocation3.y+50 && playerHealth < 50)
      {
        health3IsPickedup = true;
        playerHealth = playerHealth + 10;
      }
    }
     ////////////////////////// KEY //////////////////////////////////
     if (key_is_spawned == true && playerLocation.x > chestKeyLocation.x && playerLocation.x < chestKeyLocation.x+50 && playerLocation.y > chestKeyLocation.y-50 && playerLocation.y < chestKeyLocation.y+50)
     {
       numbers_of_key_acquired += 1;
       key_is_spawned = false;
       enemy_is_killed = false;      
     }
  }

}
