boolean gameIsOver = false;
PImage title;
PImage button;
boolean gameIsRunning = false;
int buttonX = 200;
int buttonY = 450;

class gameStates
{

  void initiateGameStates()
  {
    title = loadImage("titleScreen.png");
    button = loadImage("startButton.png");
    gameStatesFunction_MainMenu();
    gameStatesFunction_GameOver();
  }

  void gameStatesFunction_MainMenu()
  {
    if (gameIsRunning == false && gameIsOver == false)
    {
      displayMainMenu();
    }
  }

  void gameStatesFunction_GameOver()
  {
    if (gameIsOver == true)
    {
      displayGameOver();
    }
  }

  void displayMainMenu()
  {
    if (gameIsRunning == false)
    {
      image(title,0,0);
      image(button,buttonX,buttonY);

    } 
      if (mousePressed == true)
    {
      gameIsRunning = true;
    }

  }

  void displayGameOver()
  {
      noStroke();
      fill(0);
      rectMode(CENTER);
      rect(300, 300, 600, 600);

    if (mousePressed == true)
    {
      gameIsOver = false;
      playerHealth = 50;
      currentAmmo = 15;
    }
  }
  

}
