// Final Release Version 1.0

import processing.sound.*;

/*Dungeon Crawler Maze
 By [Insert our names here]
 Sources
 Music by Kevin MacLeod
 @ https://www.youtube.com/watch?v=tM5d0G0vue0
 */
Chest treasure;
Walls maze;
Player p;
Crosshair c;
pickupSystem Pickup;
boolean enemyInCrosshair = false;
Healthbar health;
PImage backgroundimage;
boolean collisionDetect;
int keyNo=1;
int treasureChestStatus=0;
int enemyNo=4;
Player_Animation animate;
Enemy[] eyes = new Enemy[8];
int enemyDamage=50;
SoundFile theme;
Key chestkey;
gameStates GameStates;
Flashlight light;


void setup() {
  size(600, 600);
  GameStates = new gameStates();
  animate = new Player_Animation();
  c = new Crosshair();
  p = new Player();
  chestkey = new Key();
  Pickup = new pickupSystem();
  eyes[0] = new Enemy(540, 10, true, 0, 500);
  eyes[1] = new Enemy(20, 10, true, 0, 500);
  eyes[2] = new Enemy(100, 540, false, 100, 460);
  eyes[3] = new Enemy(200, 150, false, 150, 430);
  eyes[4] = new Enemy(300, 10, false, 100, 490);
  eyes[5] = new Enemy(350, 390, false, 150, 440);
  eyes[6] = new Enemy(300, 450, false, 150, 440);
  eyes[7] = new Enemy(150, 90, false, 150, 430);
  theme = new SoundFile(this, "Dungeon.mp3");
  theme.play();
  theme.loop();
  treasure = new Chest();
  maze = new Walls();
  health = new Healthbar();
  backgroundimage = loadImage("dungeonfloor.png");
  backgroundimage = loadImage("dungeonfloor1.png");
  animate.inital();
  treasure.initialize();
  maze.initial();
  light = new Flashlight();
  light.initialize();
  GameStates.initiateGameStates();
  maze.initial();
}


void draw() {
  imageMode(CORNER);
  background(0);
  image(backgroundimage, 0, 0);

  PImage enemyImage=loadImage("Eyeballmonster1.png");
  eyes[0].display();
  eyes[0].moveEnemy();
  eyes[1].display();
  eyes[1].moveEnemy();
  eyes[2].display();
  eyes[2].moveEnemy();
  eyes[3].display();
  eyes[3].moveEnemy();
  eyes[4].display();
  eyes[4].moveEnemy();
  eyes[5].display();
  eyes[5].moveEnemy();
  eyes[6].display();
  eyes[6].moveEnemy();
  eyes[7].display();
  eyes[7].moveEnemy();
  treasure.display();
  Pickup.initiatePickupSystem();
  chestkey.initiateChestKey();
  p.playerCharacter();
  // call the crosshair class
  c.enableCrosshair(); 
  p.playerCharacter();
  maze.display();
  imageMode(CENTER);
  light.display();
  health.display();
  imageMode(CORNER);
  collisionDetect=collision(p.playerImage, p.playerLocation.x, p.playerLocation.y, maze.Walls, 0, 0);
  if (collisionDetect==true) {
    p.playerLocation.x=300;
    p.playerLocation.y=300;
  }
  collisionDetect=collision(p.playerImage, p.playerLocation.x, p.playerLocation.y, treasure.closed, treasure.x, treasure.y);
  if (collisionDetect==true && keyNo>0) {
    keyNo-=1;
    treasureChestStatus=1;
  }

  //Detects enemy collision
  for (int i=0; i<4; i++) {
    collisionDetect=collision(p.playerImage, p.playerLocation.x, p.playerLocation.y, enemyImage, eyes[i].x1, eyes[i].y1);
    if (collisionDetect==true) {
      p.playerLocation.x=300;
      p.playerLocation.y=300;
      playerHealth-=enemyDamage;

      if (playerHealth < 0)
      {
        playerHealth = 0;
      }

      if (playerHealth <= 0)
      {
        gameIsOver = true;        
      }
    }
  }



  GameStates.displayMainMenu();
}

// assign player's input on the keyboard
void keyPressed()
{
  if (key == 'a' || key == 'A')
  {
    leftMovement = true;
  }  


  if (key == 'd' || key == 'D')
  {
    rightMovement = true;
  }

  if (key == 'w' || key == 'W')
  {
    upMovement = true;
  }

  if (key == 's' || key == 'S')
  {
    downMovement = true;
  }

  if (key == 'r' || key == 'R')
  {
    currentAmmo = 15;
  }
}

// preventing player's input from running forever. If the key's released, input will stop  
void keyReleased()
{
  if (key == 'a' || key == 'A')
  {
    leftMovement = false;
  }

  if (key == 'd' || key == 'D')
  {
    rightMovement = false;
  }

  if (key == 'w' || key == 'W')
  {
    upMovement = false;
  }

  if (key == 's' || key == 'S')
  {
    downMovement = false;
  }
  {
    if (key == 's' || key == 'S')
    {
      downMovement = false;
    }

    if (key == 'r' || key == 'R')
    {
    }
  }
}
