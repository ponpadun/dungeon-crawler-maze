PVector chestKeyLocation = new PVector (300,275);

int numbers_of_key_acquired = 0;
boolean key_is_spawned = false;

class Key 
{
  PImage chestkey;
  float x = random(10, 590);
  float y = random(10, 590);
  boolean playerhaskey = false;

  void initiateChestKey() 
  {
    chestkey = loadImage("key.png");
    spawnChestKey();
  }

  void spawnChestKey()
  {
    if (enemy_is_killed == true)
    {    
    image(chestkey,chestKeyLocation.x, chestKeyLocation.y);
    }  
  }
}
