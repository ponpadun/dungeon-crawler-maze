class Player_Animation{
int maxImage = 4;
int imageIndex = 0;

PImage[] playerfront = new PImage[maxImage];
PImage[] playerleft = new PImage[maxImage];
PImage[] playerright = new PImage[maxImage];
PImage[] playerback = new PImage[maxImage];

void inital(){
for (int i = 0; i < playerfront.length; i++) {
  playerfront[i] = loadImage("player" + i + ".png");
  }
  for (int i = 0; i < playerleft.length; i++) {
  playerleft[i] = loadImage("playerleft" + i + ".png");
  }
    for (int i = 0; i < playerright.length; i++) {
  playerright[i] = loadImage("playerright" + i + ".png");
  }
   for (int i = 0; i < playerback.length; i++) {
  playerback[i] = loadImage("playerback" + i + ".png");
  }
 
 
}

void playerfront(){
image(playerfront[imageIndex], p.playerLocation.x, p.playerLocation.y,40,40);
imageIndex = (imageIndex + 1) % playerfront.length;
}

void playerleft(){
imageIndex = (imageIndex + 1) % playerleft.length;
image(playerleft[imageIndex], p.playerLocation.x, p.playerLocation.y,40,40);

}

void playerright(){
image(playerright[imageIndex], p.playerLocation.x, p.playerLocation.y,40,40);
imageIndex = (imageIndex + 1) % playerright.length;

}

void playerback(){
image(playerback[imageIndex],p.playerLocation.x, p.playerLocation.y,40,40);
imageIndex = (imageIndex + 1) % playerback.length;

}
}
