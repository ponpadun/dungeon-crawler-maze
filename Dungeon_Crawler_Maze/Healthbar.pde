float playerHealth = 50;

class Healthbar
{
  float x1 = 90;
  float x2 = 90;
  float y1 = 10;
  float y2 = 10;
  color backing = #808080;
  color life = #FF0000;

  void display()
  {
    fill(#8b0000);
    rect(x1, y1, x2, y2);
    stroke(0);
    fill(life);
    rect(x1, y1, map(playerHealth, 0, 30, 0, x2), y2);
  }

}
