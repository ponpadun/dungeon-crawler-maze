class Chest{
PImage closed;
PImage open;
int x = 280;
int y = 210;

void initialize(){
closed = loadImage("Chest.png");
open = loadImage("chestopen.png");

}

void display()
{ 
  if(numbers_of_key_acquired == 4){
  treasureChestStatus = 1;
  }
  if (treasureChestStatus == 0)
  {
    image(closed,x,y);
  }
  
  if (treasureChestStatus == 1)
  {
    image(open,x,y);
  }
  

}
}
