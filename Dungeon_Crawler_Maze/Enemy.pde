class Enemy {

  boolean moveForward = true;
  boolean alive = true;


  //variables for enemy image
  int maxImages = 8;
  int imageIndex = 0;
  PImage[] eyeball = new PImage[maxImages];

  //variables for enemy locations
  int x1 = 540;
  int y1 = 10;


  //variables for image diameter
  float diam1 = 40;
  float diam2 = 40;

  //variables for limit
  int limitMin;
  int limitMax;

  //move direction
  boolean verticalMovement;



  Enemy(int x, int y, boolean movesVertically, int pixelLimitMin, int pixelLimitMax) {
    //loads image
    for (int i = 0; i < eyeball.length; i++) {
      eyeball[i] = loadImage("Eyeballmonster" + i + ".png");
    }
    x1 = x;
    y1 = y;

    limitMin = pixelLimitMin;
    limitMax = pixelLimitMax;

    verticalMovement = movesVertically;
  }


  void display() {
    //displays the enemy sprite
    if (alive)
    {
      image(eyeball[imageIndex], x1, y1, diam1, diam2);
      imageIndex = (imageIndex + 1) % eyeball.length;
    }
  }

  void moveEnemy() {
    if (verticalMovement) {

      if (y1 == limitMax) {
        moveForward = false;
      } else if (y1 == limitMin) {
        moveForward = true;
      }

      if (moveForward) {
        y1 += 1;
      } else {
        y1 -= 1;
      }
 
    } else {
      if (x1 == limitMax) {
        moveForward = false;
      } else if (x1 == limitMin) {
        moveForward = true;
      }

      if (moveForward)
      {
        x1 += 1;
      } else
      {
        x1 -= 1;
      }
    }
  }
}
