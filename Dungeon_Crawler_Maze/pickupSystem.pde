PVector ammoPickupLocation1 = new PVector (100, 300);
PVector ammoPickupLocation2 = new PVector (400, 300);
PVector ammoPickupLocation3 = new PVector (300, 90);

PVector healthPickupLocation1 = new PVector (500, 300);
PVector healthPickupLocation2 = new PVector (180,550);
PVector healthPickupLocation3 = new PVector (29,20);
//images
PImage potion;
PImage arrow;

PVector ammoCounter1 = new PVector ();
PVector ammoCounter2 = new PVector ();
PVector ammoCounter3 = new PVector ();


float ammoCounterSizeRadius = 6;

boolean ammo1IsPickedup = false;
boolean ammo2IsPickedup = false;
boolean ammo3IsPickedup = false;

boolean health1IsPickedup = false;
boolean health2IsPickedup = false;
boolean health3IsPickedup = false;

class pickupSystem
{

  void initiatePickupSystem()
  {
    potion = loadImage("health.png");
    arrow = loadImage("ammo.png");
    spawnAmmoPickup();
    spawnHealthPickup();
    drawAmmoCount();
  }
  // draw ammo counter on the screen,  the count is right next to the reticle for easy viewing
  void drawAmmoCount()
  {
    if (currentAmmo == 15)
    {
      strokeWeight(1);
      stroke(0);
      fill(255);
      circle(mouseX+8,mouseY-10,ammoCounterSizeRadius);
      circle(mouseX+8,mouseY,ammoCounterSizeRadius);
      circle(mouseX+8,mouseY+10,ammoCounterSizeRadius);
    }
    
    else if (currentAmmo == 10)
    {
      strokeWeight(1);
      stroke(0);
      fill(255);
      circle(mouseX+8,mouseY-10,ammoCounterSizeRadius);
      circle(mouseX+8,mouseY,ammoCounterSizeRadius);
    }
    
    else if (currentAmmo >= 1)
    {
      strokeWeight(1);
      stroke(0);
      fill(255);
      circle(mouseX+8,mouseY-10,ammoCounterSizeRadius);
    }
  }

  void spawnAmmoPickup()
  {
    strokeWeight(3);
    stroke(0);
    fill(0,0,255);

    if (ammo1IsPickedup == false)
    {
      image(arrow,ammoPickupLocation1.x,ammoPickupLocation1.y,40,40);
    }
    
    if (ammo2IsPickedup == false)
    {

      image(arrow,ammoPickupLocation2.x,ammoPickupLocation2.y,40,40);

    }
    
    if (ammo3IsPickedup == false)
    {
      image(arrow,ammoPickupLocation3.x,ammoPickupLocation3.y,40,40);
    }
  }

  void spawnHealthPickup()
  {
    strokeWeight(3);
    stroke(0);
    fill(255,0,0);
    
    if (health1IsPickedup == false)
    {
     image(potion,healthPickupLocation1.x,healthPickupLocation1.y,30,30);
    }
    if (health2IsPickedup == false)
  {
    image(potion,healthPickupLocation2.x,healthPickupLocation2.y, 30,30);    
}
  if (health3IsPickedup == false)
  {
    image(potion,healthPickupLocation3.x,healthPickupLocation3.y, 30,30);    
}

  }
}
