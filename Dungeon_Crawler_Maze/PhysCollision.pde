//Code snippet and logic taken from: https://forum.processing.org/two/discussion/5282/pixel-level-collision-detection

// A pixel width an alpha level below this value is
// considered transparent.
final int alpha = 20;
 
boolean collision(PImage imgA, float aix, float aiy, PImage imgB, float bix, float biy) {
  
  //initiate variables for tracking image coordinates
  
  int topA, botA, leftA, rightA;
  int topB, botB, leftB, rightB;
  int topO, botO, leftO, rightO;
  int ax, ay;
  int bx, by;
  int APx, APy, ASx, ASy;
  int BPx, BPy; //, BSx, BSy;
 
  topA   = (int) aiy;
  botA   = (int) aiy + imgA.height;
  leftA  = (int) aix;
  rightA = (int) aix + imgA.width;
  topB   = (int) biy;
  botB   = (int) biy + imgB.height;
  leftB  = (int) bix;
  rightB = (int) bix + imgB.width;
 
  if (botA <= topB  || botB <= topA || rightA <= leftB || rightB <= leftA)
    return false;
 
  // If we get here, we know that there is an overlap
  // So we work out where the sides of the overlap are
  leftO = (leftA < leftB) ? leftB : leftA;
  rightO = (rightA > rightB) ? rightB : rightA;
  botO = (botA > botB) ? botB : botA;
  topO = (topA < topB) ? topB : topA;
 
  // P is the top-left, S is the bottom-right of the overlap
  APx = leftO-leftA;   
  APy = topO-topA;
  ASx = rightO-leftA;  
  ASy = botO-topA-1;
  BPx = leftO-leftB;   
  BPy = topO-topB;
 
  int widthO = rightO - leftO;
  boolean foundCollision = false;
 
  // Images to test
  imgA.loadPixels();
  imgB.loadPixels();
 
  boolean pixelAtransparent, pixelBtransparent = true;
 
  // Get start pixel positions
  int pA = (APy * imgA.width) + APx;
  int pB = (BPy * imgB.width) + BPx;
 
  ax = APx; 
  ay = APy;
  bx = BPx; 
  by = BPy;
  for (ay = APy; ay < ASy; ay++) {
    bx = BPx;
    for (ax = APx; ax < ASx; ax++) {
      pixelAtransparent = alpha(imgA.pixels[pA]) < alpha;
      pixelBtransparent = alpha(imgB.pixels[pB]) < alpha;
 
      if (!pixelAtransparent && !pixelBtransparent) {
        foundCollision = true;
        break;
      }
      pA ++;
      pB ++;
      bx++;
    }
    if (foundCollision) break;
    pA = pA + imgA.width - widthO;
    pB = pB + imgB.width - widthO;
    by++;
  }
  return foundCollision;
}
