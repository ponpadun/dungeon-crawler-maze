// creating PVector as a variable for crosshair movement
PVector crosshairLocation = new PVector (mouseX, mouseY);

boolean canShoot;

int currentAmmo = 15;
int maxAmmo = 15;

boolean enemy_is_killed = false;

class Crosshair
{
  // create a variable for the size of a reticle
  float reticleSize = 50;

  // main function
  void enableCrosshair()
  {
    if (currentAmmo >=1 && gameIsRunning == true)
    {
      canShoot = true;
      normalCrosshairColor();
    } else if (currentAmmo < 1)
    {
      canShoot = false; 
      redCrosshairColor();
    }

    shooting();
  }

  void shooting()
  {
    if (mousePressed == true && canShoot == true)
    {
      hitEffect();
      checkEnemyHit();
      ammoCalculation();
      if (currentAmmo < 1)
      {
        currentAmmo = 0;
      } else if (currentAmmo > maxAmmo)
      {
        currentAmmo = maxAmmo;
      }
    }
  }

  void ammoCalculation()
  {
    currentAmmo = currentAmmo -1;
  }

  // normal crosshair
  void normalCrosshairColor()
  {
    strokeWeight(3.5);
    stroke(255, 255, 255);
    noFill();

    circle(mouseX, mouseY, reticleSize);

    noStroke();
    fill(255);
    circle(mouseX, mouseY, reticleSize/6);    

    strokeWeight(3);
    stroke(255);
    line(mouseX, mouseY-15, mouseX, mouseY-30);
    line(mouseX, mouseY+15, mouseX, mouseY+30);
    line(mouseX-15, mouseY, mouseX-30, mouseY);
    line(mouseX+15, mouseY, mouseX+30, mouseY);
  }

  // normal crosshair
  void redCrosshairColor()
  {
    strokeWeight(3.5);
    stroke(255, 0, 0);
    noFill();

    circle(mouseX, mouseY, reticleSize);

    noStroke();
    fill(255, 0, 0);
    circle(mouseX, mouseY, reticleSize/6);    

    strokeWeight(3);
    stroke(255, 0, 0);
    line(mouseX, mouseY-15, mouseX, mouseY-30);
    line(mouseX, mouseY+15, mouseX, mouseY+30);
    line(mouseX-15, mouseY, mouseX-30, mouseY);
    line(mouseX+15, mouseY, mouseX+30, mouseY);
  }

  // crosshair for when player shoots enemies
  void hitEffect()
  {
    // hit registration effect
    strokeWeight(5);
    stroke(255, 0, 0, 400);
    line(mouseX-20, mouseY-20, mouseX+20, mouseY+20);
    line(mouseX+20, mouseY-20, mouseX-20, mouseY+20);
  }

  void checkEnemyHit()
  {
    for (int i =0; i < eyes.length; i++) {
      
      if (pointRect(mouseX, mouseY, eyes[i].x1, eyes[i].y1, eyes[i].diam1, eyes[i].diam2))
      {
        //here is where you should remove the enemy from the arraylist
        eyes[i].alive = false;
        enemy_is_killed = true;
        key_is_spawned = true;        
      }
    }
  }


  boolean pointRect(float px, float py, float rx, float ry, float rw, float rh) {

    // is the point inside the rectangle's bounds?
    if (px >= rx &&        // right of the left edge AND
      px <= rx + rw &&   // left of the right edge AND
      py >= ry &&        // below the top AND
      py <= ry + rh) {   // above the bottom
      return true;
    }
    return false;
  }
}
